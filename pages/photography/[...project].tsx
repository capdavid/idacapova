import cn from 'classnames'
import React, { useRef, useEffect, useState } from 'react'
import Link from 'next/link'
import { Image, Toggle, Video } from 'components'
import { InferGetStaticPropsType } from 'next'
import { useRouter } from 'next/router'
import { fetchContent, ProjectsData } from 'utils/fetchContent'
import Modal from 'react-modal'

const PAGE_TITLE = 'photography'

export const ProjectPage = ({ data }: InferGetStaticPropsType<typeof getStaticProps>) => {
  const router = useRouter()
  const [categoryName, projectName] = router?.query?.project || []
  const currentCategory = data.find((el) => el.name === categoryName)
  const currentCategoryIndex = data.indexOf(currentCategory!)

  const currentProject = currentCategory?.projects.find((el) => el.slug === projectName)
  const images =
    typeof currentProject?.images === 'object' ? currentProject?.images : [currentProject?.images]

  const galleryRef = useRef<HTMLElement>(null)

  useEffect(() => {
    const handleHorizontalScroll = (
      event: WheelEvent,
      target: React.MutableRefObject<HTMLElement>['current']
    ) => {
      const toLeft = event.deltaY < 0 && target.scrollLeft > 0
      const toRight =
        event.deltaY > 0 && target.scrollLeft < target.scrollWidth - target.clientWidth

      if (toLeft || toRight) {
        event.preventDefault()
        target.scrollLeft += event.deltaY
      }
    }

    if (typeof galleryRef !== undefined && galleryRef !== null && 'current' in galleryRef) {
      const target = galleryRef?.current
      target!.addEventListener('wheel', (e) => handleHorizontalScroll(e, target!))
      return () => {
        target!.removeEventListener('wheel', (e) => handleHorizontalScroll(e, target!))
      }
    }
  }, [galleryRef])

  const [previewOpen, setPreviewOpen] = useState(false)

  return (
    <div className="flex flex-col justify-between h-screen text-black">
      <Link legacyBehavior href="/photography">
        <a className="font-arial font-bold text-3/2em tracking-widest uppercase fixed top-64 sm:top-38 left-32 sm:left-48">
          {PAGE_TITLE}
        </a>
      </Link>
      <Link legacyBehavior href="/">
        <a className="font-arial font-bold text-3/2em tracking-widest uppercase fixed top-38 right-38">
          IDA CAPOVA
        </a>
      </Link>
      <Link legacyBehavior href="/graphic-design">
        <a className="fixed top-38 left-32 sm:left-auto sm:right-1/2 mr-48 z-20">
          <Toggle project />
        </a>
      </Link>
      <section className="flex mx-32 sm:mx-48 pt-114">
        <div className="flex flex-col flex-1">
          <div className="flex justify-between flex-col sm:flex-row">
            <Link legacyBehavior href={`/${PAGE_TITLE}#${categoryName}`}>
              <a
                className={cn(
                  'font-arial font-bold uppercase tracking-wider border-b-2 border-black'
                )}
              >
                {categoryName}
              </a>
            </Link>
            {currentCategory && (
              <span className="font-arial font-bold tracking-wider mr-48 border-b-2 border-black mt-16 sm:mt-0">
                {currentProject?.title}
              </span>
            )}
          </div>
        </div>
        <div className="flex-1" />
      </section>
      <section
        className="my-36 flex items-center overflow-x-auto scrollbar-light overflow-y-hidden"
        ref={galleryRef}
      >
        {/* @ts-ignore */}
        <Modal
          isOpen={previewOpen}
          //class is defined in global.css
          overlayClassName={{
            base: `modal ${'bg-black'}`,
            afterOpen: 'afterOpen',
            beforeClose: 'beforeClose',
          }}
          ariaHideApp={false}
          className="h-9/10 max-h-1200 max-w-9/10 focus:outline-none flex items-center gap-16"
          onRequestClose={() => setPreviewOpen(false)}
        >
          {!!images?.length &&
            images.map((image) =>
              image?.endsWith('mp4') ? (
                <Video
                  key={image}
                  src={image}
                  blackBackground
                  className="h-full w-auto cursor-pointer mr-48 last:pr-48"
                />
              ) : (
                <img
                  className="max-h-full max-w-full"
                  onClick={() => setPreviewOpen(false)}
                  src={image}
                />
              )
            )}
        </Modal>
        <div
          className={cn(
            'w-1/2 flex items-center',
            categoryName === 'contact' ? 'justify-end' : 'justify-center'
          )}
        >
          <div className="mx-auto pl-32 sm:px-96">
            <p className="tracking-wide font-semibold">{currentProject?.description}</p>
            {currentProject?.description2 && (
              <p className="tracking-wide font-semibold mt-1rem">{currentProject?.description2}</p>
            )}
            <p className="tracking-wide font-light mt-2rem">{currentProject?.summary}</p>
            <p className="tracking-wide font-normal mt-3rem">{currentProject?.location}</p>
          </div>
        </div>
        <div
          className="w-1/2 flex space-x-48 pl-48 max-h-700 items-center"
          style={{
            height: `calc(100vh - 224px - ${data.length + 2} * 1.5em - ${
              data.length + 2
            } * 8px - 4px)`,
          }}
        >
          {!!images.length &&
            images.map((image) =>
              image?.endsWith('mp4') ? (
                <Video
                  key={image}
                  src={image}
                  blackBackground
                  className="h-full w-auto cursor-pointer mr-48 last:pr-48"
                />
              ) : (
                <img
                  className="h-full w-auto cursor-pointer mr-48 last:pr-48"
                  onClick={() => setPreviewOpen(true)}
                  src={image}
                />
              )
            )}
        </div>
      </section>
      <section className="px-32 sm:px-48 mb-38 w-full">
        <div className="flex flex-col items-start">
          {data.map(({ name, title }, index) => {
            return (
              currentCategoryIndex !== index && (
                <Link legacyBehavior key={name} href={`/${PAGE_TITLE}#${name}`}>
                  <a
                    className={cn(
                      'font-arial font-bold uppercase tracking-wider my-4 border-b-2 border-transparent hover:border-black'
                    )}
                  >
                    {title}
                  </a>
                </Link>
              )
            )
          })}
          {categoryName !== 'about' && (
            <Link legacyBehavior href={`/${PAGE_TITLE}#about`}>
              <a
                className={cn(
                  'font-arial font-bold uppercase tracking-wider my-4 border-b-2 border-transparent hover:border-black'
                )}
              >
                about
              </a>
            </Link>
          )}
          {categoryName !== 'contact' && (
            <Link legacyBehavior href={`/${PAGE_TITLE}#contact`}>
              <a
                className={cn(
                  'font-arial font-bold uppercase tracking-wider mt-4 border-b-2 border-transparent hover:border-black'
                )}
              >
                contact
              </a>
            </Link>
          )}
        </div>
      </section>
      <span className="font-light tracking-wider absolute bottom-38 right-38">© 2021</span>
    </div>
  )
}

export async function getStaticPaths() {
  const data: ProjectsData[] = fetchContent('photography')

  const paths: any = []

  data.forEach((category) =>
    category.projects.forEach((project) =>
      paths.push({
        params: { project: [category.name, project.slug] },
      })
    )
  )

  return { paths, fallback: false }
}

export const getStaticProps = async (ctx: any) => {
  const data: ProjectsData[] = fetchContent('photography')

  return { props: { data } }
}

export default ProjectPage
