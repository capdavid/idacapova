import React, { useState } from 'react'
import Link from 'next/link'
import cn from 'classnames'
import Head from 'next/head'
import { attributes } from 'content/home.md'
import { Video } from 'components'

const index: React.FC = () => {
  const [graphicDesign, setGraphicDesign] = useState(false)
  const [photography, setPhotography] = useState(false)
  return (
    <>
      <Head>
        <script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
      </Head>
      <div className="flex flex-col md:hidden " style={{ height: '100dvh' }}>
        <section className="flex-1 bg-white flex-shrink-0 h-full" style={{ height: '50dvh' }}>
          <Link legacyBehavior href="/photography">
            <a className="w-full h-full flex flex-col justify-end align-middle p-16">
              <img className="h-4/5 object-contain mx-auto self-center" src={attributes.image[1]} />
              <p className="text-black font-arial tracking-widest text-xl font-bold text-3/2em mt-16 text-center">
                PHOTOGRAPHY
              </p>
            </a>
          </Link>
        </section>
        <section className="flex-1 bg-black flex-shrink-0 h-full" style={{ height: '50dvh' }}>
          <Link legacyBehavior href="/graphic-design">
            <a className="w-full h-full flex flex-col justify-start align-middle p-16">
              <p className="text-white font-arial tracking-widest text-xl font-bold text-3/2em text-center mb-16">
                GRAPHIC DESIGN
              </p>
              <img className="h-4/5 object-contain mx-auto self-center" src={attributes.image[0]} />
            </a>
          </Link>
        </section>
      </div>
      <div className="hidden md:block">
        <h1 className="font-arial font-bold uppercase text-3/2em tracking-widest absolute top-38 left-48 z-20 text-white">
          portfolio
        </h1>
        <span className="font-arial font-bold uppercase text-3/2em tracking-widest absolute top-38 right-38 z-20 text-black">
          IDA CAPOVA
        </span>
        <div
          className="opacity-0 h-full bg-pink-400 w-5/12 absolute z-10 cursor-pointer"
          onMouseEnter={() => setGraphicDesign(true)}
          onMouseLeave={() => setGraphicDesign(false)}
        >
          <Link legacyBehavior href="/graphic-design">
            <a className="w-full h-full block"></a>
          </Link>
        </div>
        <div
          className="opacity-0 h-full bg-pink-400 w-5/12 right-0 absolute z-10"
          onMouseEnter={() => setPhotography(true)}
          onMouseLeave={() => setPhotography(false)}
        >
          <Link legacyBehavior href="/photography">
            <a className="w-full h-full block"></a>
          </Link>
        </div>
        <div className="w-screen h-screen flex">
          <section
            className={cn(
              'w-1/2 h-full flex items-center justify-around',
              photography ? 'bg-white' : 'bg-black'
            )}
          >
            {photography ? (
              <p className="text-black font-arial tracking-widest text-xl font-bold text-3/2em">
                PHOTOGRAPHY
              </p>
            ) : (
              <div className="h-3/5">
                {attributes.image[0].endsWith('mp4') ? (
                  <Video
                    muted
                    autoPlay
                    blackBackground
                    src={attributes.image[0]}
                    className="h-full w-auto cursor-pointer"
                  />
                ) : (
                  <img className="h-full" src={attributes.image[0]} />
                )}
              </div>
            )}
          </section>
          <section
            className={cn(
              'w-1/2 h-full flex items-center justify-around',
              graphicDesign ? 'bg-black' : 'bg-white'
            )}
          >
            {graphicDesign ? (
              <p className="text-white font-arial tracking-widest text-xl font-bold text-3/2em">
                GRAPHIC DESIGN
              </p>
            ) : (
              <div className="h-3/5">
                {attributes.image[1].endsWith('mp4') ? (
                  <Video
                    muted
                    autoPlay
                    blackBackground
                    src={attributes.image[1]}
                    className="h-full w-auto cursor-pointer"
                  />
                ) : (
                  <img className="h-full" src={attributes.image[1]} />
                )}
              </div>
            )}
          </section>
        </div>
      </div>
    </>
  )
}

export default index
