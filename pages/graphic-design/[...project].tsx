import cn from 'classnames'
import React, { useRef, useEffect } from 'react'
import Link from 'next/link'
import { Image, Video, Toggle } from 'components'
import { InferGetStaticPropsType } from 'next'
import { useRouter } from 'next/router'
import { fetchContent, ProjectsData } from 'utils/fetchContent'

const PAGE_TITLE = 'graphic design'
const PAGE_SLUG = 'graphic-design'

export const ProjectPage = ({ data }: InferGetStaticPropsType<typeof getStaticProps>) => {
  const router = useRouter()
  const [categoryName, projectName] = router?.query?.project || []
  const currentCategory = data.find((el) => el.name === categoryName)
  const currentCategoryIndex = data.indexOf(currentCategory!)

  const currentProject = currentCategory?.projects.find((el) => el.slug === projectName)

  const galleryRef = useRef<HTMLElement>(null)

  useEffect(() => {
    const handleHorizontalScroll = (
      event: WheelEvent,
      target: React.MutableRefObject<HTMLElement>['current']
    ) => {
      const toLeft = event.deltaY < 0 && target.scrollLeft > 0
      const toRight =
        event.deltaY > 0 && target.scrollLeft < target.scrollWidth - target.clientWidth

      if (toLeft || toRight) {
        event.preventDefault()
        target.scrollLeft += event.deltaY
      }
    }

    if (typeof galleryRef !== undefined && galleryRef !== null && 'current' in galleryRef) {
      const target = galleryRef?.current
      target!.addEventListener('wheel', (e) => handleHorizontalScroll(e, target!))
      return () => {
        target!.removeEventListener('wheel', (e) => handleHorizontalScroll(e, target!))
      }
    }
  }, [galleryRef])

  return (
    <div className="h-screen w-screen bg-black">
      <div className="flex flex-col justify-between h-full text-white">
        <Link legacyBehavior href={'/' + PAGE_SLUG}>
          <a className="font-arial font-bold text-3/2em tracking-widest uppercase fixed top-64 sm:top-38 left-32 sm:left-48">
            {PAGE_TITLE}
          </a>
        </Link>
        <Link legacyBehavior href="/">
          <a className="font-arial font-bold text-3/2em tracking-widest uppercase fixed top-38 right-38">
            IDA CAPOVA
          </a>
        </Link>
        <Link legacyBehavior href="/photography">
          <a className="fixed top-38 left-32 sm:left-auto sm:right-1/2 mr-48 z-20">
            <Toggle photography project />
          </a>
        </Link>
        <section className="flex mx-32 sm:mx-48 pt-114">
          <div className="flex flex-col flex-1">
            <div className="flex justify-between">
              <Link legacyBehavior href={`/${PAGE_SLUG}#${categoryName}`}>
                <a
                  className={cn(
                    'font-arial font-bold uppercase tracking-wider border-b-2 border-white'
                  )}
                >
                  {categoryName}
                </a>
              </Link>
              {currentCategory && (
                <span className="font-arial font-bold tracking-wider mr-48 border-b-2 border-white">
                  {currentProject?.title}
                </span>
              )}
            </div>
          </div>
          <div className="flex-1" />
        </section>
        <section
          className="my-36 flex items-center overflow-x-auto scrollbar-dark overflow-y-hidden"
          ref={galleryRef}
        >
          <div
            className={cn(
              'w-1/2 flex items-center',
              categoryName === 'contact' ? 'justify-end' : 'justify-center'
            )}
          >
            <div className="mx-auto pl-32 sm:px-96">
              <p className="tracking-wide font-semibold">{currentProject?.description}</p>
              {currentProject?.description2 && (
                <p className="tracking-wide font-semibold mt-1rem">
                  {currentProject?.description2}
                </p>
              )}
              <p className="tracking-wide font-light mt-2rem">{currentProject?.summary}</p>
              <p className="tracking-wide font-normal mt-3rem">{currentProject?.location}</p>
            </div>
          </div>
          <div
            className="w-1/2 flex pl-48 max-h-700"
            style={{
              height: `calc(100vh - 224px - ${data.length + 2} * 1.5em - ${
                data.length + 2
              } * 8px - 15px)`,
            }}
          >
            {typeof currentProject?.images === 'object' &&
              currentProject?.images.map((image) =>
                image.endsWith('mp4') ? (
                  <Video
                    key={image}
                    src={image}
                    blackBackground
                    className="h-full w-auto cursor-pointer mr-48 last:pr-48"
                  />
                ) : (
                  <Image
                    key={image}
                    src={image}
                    blackBackground
                    className="h-full w-auto cursor-pointer mr-48 last:pr-48"
                  />
                )
              )}
            {typeof currentProject?.images === 'string' &&
              (currentProject?.images.endsWith('mp4') ? (
                <Video
                  autoPlay
                  blackBackground
                  src={currentProject?.images}
                  className="h-full w-auto cursor-pointer last:pr-48"
                />
              ) : (
                <Image
                  blackBackground
                  src={currentProject?.images}
                  className="h-full w-auto cursor-pointer last:pr-48"
                />
              ))}
          </div>
        </section>
        <section className="px-32 sm:px-48 mb-38 w-full">
          <div className="flex flex-col items-start">
            {data.map(({ name, title }, index) => {
              return (
                currentCategoryIndex !== index && (
                  <Link legacyBehavior key={name} href={`/${PAGE_SLUG}#${name}`}>
                    <a
                      className={cn(
                        'font-arial font-bold uppercase tracking-wider my-4 border-b-2 border-transparent hover:border-white'
                      )}
                    >
                      {title}
                    </a>
                  </Link>
                )
              )
            })}
            {categoryName !== 'about' && (
              <Link legacyBehavior href={`/${PAGE_SLUG}#about`}>
                <a
                  className={cn(
                    'font-arial font-bold uppercase tracking-wider my-4 border-b-2 border-transparent hover:border-white'
                  )}
                >
                  about
                </a>
              </Link>
            )}
            {categoryName !== 'contact' && (
              <Link legacyBehavior href={`/${PAGE_SLUG}#contact`}>
                <a
                  className={cn(
                    'font-arial font-bold uppercase tracking-wider mt-4 border-b-2 border-transparent hover:border-white'
                  )}
                >
                  contact
                </a>
              </Link>
            )}
          </div>
        </section>
        <span className="font-light tracking-wider absolute bottom-38 right-38">© 2021</span>
      </div>
    </div>
  )
}

export async function getStaticPaths() {
  const data: ProjectsData[] = fetchContent('design')

  const paths: any = []

  data.forEach((category) =>
    category.projects.forEach((project) =>
      paths.push({
        params: { project: [category.name, project.slug] },
      })
    )
  )

  return { paths, fallback: false }
}

export const getStaticProps = async (ctx: any) => {
  const data: ProjectsData[] = fetchContent('design')

  return { props: { data } }
}

export default ProjectPage
