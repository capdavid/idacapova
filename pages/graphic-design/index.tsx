import cn from 'classnames'
import React, { useEffect, useState, useCallback } from 'react'
import { InferGetStaticPropsType } from 'next'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { fetchContent, ProjectsData, Project } from 'utils/fetchContent'
import { attributes as designInfo } from 'content/designInfo.md'
import Scroll from 'svg/scroll.svg'
import ScrollButton from 'svg/scroll-button.svg'
import { Video, Toggle } from 'components'
import { MobileSection } from 'components/MobileSection'

const PAGE_TITLE = 'graphic design'
const PAGE_SLUG = 'graphic-design'

interface AboutContactSectionProps {
  numberOfCategories: number
  lockedPosition: number
  selected: boolean
  title: string
  pageTitle: string
  image?: string
  data: Record<string, string>
}
const AboutContactSection: React.FC<AboutContactSectionProps> = ({
  numberOfCategories,
  lockedPosition,
  selected,
  title,
  pageTitle,
  data,
}) => {
  return (
    <>
      <div
        id={title}
        className={cn(
          'flex justify-between py-4 last:pb-0 transition-transform duration-500 relative'
        )}
        style={{
          transform:
            lockedPosition > 0
              ? `translate(0, calc(100vh - 156px - ${numberOfCategories} * 1.5em - ${numberOfCategories} * 8px))`
              : 'none',
        }}
      >
        <Link legacyBehavior href={`/${pageTitle}#${title}`}>
          <a
            className={cn(
              'font-arial font-bold uppercase tracking-wider border-b-2 border-transparent hover:border-black',
              selected && 'border-b-2 border-black'
            )}
          >
            {title}
          </a>
        </Link>
        <div
          className={cn('space-y-6 flex flex-col items-end absolute right-0', !selected && 'h-0')}
        ></div>
      </div>
      <div
        className={cn(
          'fixed top-1/2 transform -translate-y-1/2 w-1/2 pl-48 pr-144',
          !selected && 'h-0 hidden'
        )}
        style={{
          top: `calc((110px + ${numberOfCategories - 3} * 1.5em + ${
            numberOfCategories - 1
          } * 10px) + (100vh - 110px - ${numberOfCategories} * 1.5em - ${numberOfCategories} * 8px - 38px ) / 2)`,
        }}
      >
        {title === 'about' && (
          <>
            <p className="tracking-wide font-semibold">{data.aboutDescription}</p>
            <p className="tracking-wide font-semibold mt-1rem">{data.aboutDescription2}</p>
            <p className="tracking-wide font-light mt-2rem">{data?.aboutSummary}</p>
            <p className="tracking-wide font-normal mt-3rem">{data?.aboutLocation}</p>
          </>
        )}
        {title === 'contact' && (
          <div className="flex flex-col items-end">
            <p className="tracking-wide font-semibold">{data?.contactName}</p>
            <p className="tracking-wide font-semibold mt-1rem">{data?.contactEmail}</p>
            <p className="tracking-wide font-normal mt-3rem">{data?.contactInstagram}</p>
          </div>
        )}
      </div>
    </>
  )
}

interface SectionProps {
  title: string
  name: string
  projects: Project[]
  selected: boolean
  lockedPosition: number
  setPreview: (imageUrl: string, projectUrl: string) => void
  pageTitle: string
  numberOfCategories: number
}

const Section: React.FC<SectionProps> = ({
  title,
  name,
  projects,
  selected,
  lockedPosition,
  setPreview,
  pageTitle,
  numberOfCategories,
}) => {
  return (
    <>
      <div
        id={name}
        className={cn(
          'flex justify-between py-4 last:pb-0 transition-transform duration-500 relative'
        )}
        style={{
          transform:
            lockedPosition > 0
              ? `translate(0, calc(100vh - 156px - ${numberOfCategories} * 1.5em - ${numberOfCategories} * 8px))`
              : 'none',
        }}
      >
        <Link legacyBehavior href={`/${pageTitle}#${name}`}>
          <a
            className={cn(
              'font-arial font-bold uppercase tracking-wider border-b-2 border-transparent hover:border-black',
              selected && 'border-b-2 border-black'
            )}
          >
            {title}
          </a>
        </Link>
        <div
          className={cn('space-y-6 flex flex-col items-end absolute right-0', !selected && 'h-0')}
        >
          {projects.map((project) => (
            <Link legacyBehavior key={project.slug} href={`/${pageTitle}/${name}/${project.slug}`}>
              <a
                className={cn(
                  'tracking-wider transition-opacity duration-500 font-bold border-b-2 border-transparent hover:border-black',
                  selected ? 'opacity-100' : 'opacity-0 pointer-events-none'
                )}
                onMouseEnter={() =>
                  setPreview(
                    typeof project.images === 'object'
                      ? project.images[0]
                      : (project.images as string),
                    `${name}/${project.slug}`
                  )
                }
              >
                {project.title}
              </a>
            </Link>
          ))}
        </div>
      </div>
    </>
  )
}

const design = ({ data }: InferGetStaticPropsType<typeof getStaticProps>) => {
  const [scrollPosition, setScrollPosition] = useState<number>(0)
  const categoryLength = data.length + 2

  const pushCategories = useCallback(() => {
    const allCategories = []
    data.forEach((cat) => allCategories.push('#' + cat.name))
    allCategories.push('#about')
    allCategories.push('#contact')
    return allCategories
  }, [data])

  const allCategories = pushCategories()

  const router = useRouter()
  const [routeId] = router.asPath.match(/#([a-z0-9]+)/gi) || []

  const currentCategoryIndex = allCategories.findIndex((cat) => cat === routeId)
  const scrollOffset = 600

  useEffect(() => {
    window.scrollTo(0, currentCategoryIndex * 600 + 1)
  }, [currentCategoryIndex, scrollOffset])

  useEffect(() => {
    document.addEventListener('scroll', () =>
      setScrollPosition((c) =>
        Math.floor((window.scrollY >= 0 ? window.scrollY : 1) / scrollOffset)
      )
    )
  }, [scrollOffset])

  const [preview, setPreview] = useState<[string, string]>()

  useEffect(() => {
    const selectedCategory =
      data[scrollPosition <= data.length - 1 ? scrollPosition : data.length - 1]
    const selectedCategoryName = selectedCategory.name
    const selectedProject = selectedCategory.projects[0]
    if (scrollPosition === data.length) {
      setPreview([designInfo.aboutImage, ''])
      return
    } else if (scrollPosition + 1 === allCategories.length) {
      setPreview(undefined)
      return
    }
    setPreview([
      typeof selectedProject.images === 'object'
        ? selectedProject.images[0]
        : (selectedProject.images as string),
      `${selectedCategoryName}/${selectedProject.slug}`,
    ])
  }, [scrollPosition, setPreview, data])

  return (
    <>
      <MobileSection darkBackground data={data} />

      <div
        className="md:flex hidden"
        style={{ height: `calc(${categoryLength - 1} * 600px + 100vh + 1px)` }}
      >
        <section className="w-1/2 bg-white text-black relative">
          <Link legacyBehavior href={'/' + PAGE_SLUG}>
            <a className="font-arial font-bold text-3/2em tracking-widest uppercase sticky top-38 ml-48 z-10 float-left">
              {PAGE_TITLE}
            </a>
          </Link>
          <Link legacyBehavior href="/photography">
            <a className="sticky top-38 float-right mr-48 z-20">
              <Toggle />
            </a>
          </Link>
          <div className="fixed top-0 h-screen pt-110 w-1/2 px-48">
            {data.map(({ title, name, projects }, index) => (
              <Section
                pageTitle={PAGE_SLUG}
                title={title}
                name={name}
                key={name}
                projects={projects}
                setPreview={(imageUrl, projectUrl) => setPreview([imageUrl, projectUrl])}
                selected={index === scrollPosition}
                lockedPosition={index - scrollPosition}
                numberOfCategories={categoryLength}
              ></Section>
            ))}
            <AboutContactSection
              title="about"
              lockedPosition={data.length - scrollPosition}
              selected={data.length === scrollPosition}
              pageTitle={PAGE_SLUG}
              data={designInfo}
              numberOfCategories={categoryLength}
              image={designInfo.aboutImage}
            />
            <AboutContactSection
              title="contact"
              lockedPosition={data.length + 1 - scrollPosition}
              selected={
                scrollPosition > categoryLength - 1 ? true : categoryLength - 1 === scrollPosition
              }
              pageTitle={PAGE_SLUG}
              data={designInfo}
              numberOfCategories={categoryLength}
            />
            <div
              className={cn(
                'absolute top-1/2 transform translate -translate-y-2/4 transition-opacity duration-500',
                scrollPosition > 0 && 'opacity-0'
              )}
            >
              <Scroll className="w-5/2em stroke-black fill-transparent" />
              <ScrollButton className="w-1/3em absolute top-1em left-1/2 transform -translate-x-2/4 scroll-animation" />
            </div>
          </div>
        </section>
        <section className="w-1/2 bg-black text-white px-32 lg:px-48">
          <Link legacyBehavior href="/">
            <a className="font-arial font-bold text-3/2em tracking-widest uppercase float-right sticky top-38 -mr-10">
              IDA CAPOVA
            </a>
          </Link>
          <div className="fixed top-1/2 left-3/4 transform -translate-y-2/4 -translate-x-2/4 w-2/5 h-3/5 max-h-1200">
            {preview &&
              (preview?.[1] !== '' ? (
                <Link legacyBehavior href={`/${PAGE_SLUG}/${preview?.[1]}`}>
                  <a className="w-full flex items-center justify-center h-full">
                    {preview[0].endsWith('mp4') ? (
                      <Video
                        src={preview?.[0]}
                        autoPlay
                        unclickable
                        muted
                        className="h-full w-full max-h-1200 object-contain"
                      />
                    ) : (
                      <img
                        className="h-full w-full max-h-1200 object-contain"
                        src={preview?.[0]}
                        alt=""
                      />
                    )}
                  </a>
                </Link>
              ) : (
                <>
                  {preview[0].endsWith('mp4') ? (
                    <Video
                      src={preview?.[0]}
                      autoPlay
                      unclickable
                      muted
                      className="h-full w-full max-h-1200 object-contain"
                    />
                  ) : (
                    <img
                      className="h-full w-full max-h-1200 object-contain"
                      src={preview?.[0]}
                      alt=""
                    />
                  )}
                </>
              ))}
          </div>
        </section>
      </div>
    </>
  )
}

export const getStaticProps = async () => {
  const data: ProjectsData[] = fetchContent('design')
  return { props: { data } }
}

export default design
