import React from 'react'
import cn from 'classnames'

interface ToggleProps {
  className?: string
  photography?: boolean
  project?: boolean
}
export const Toggle: React.FC<ToggleProps> = ({ className, photography, project }) => {
  return (
    <div className={cn('box-border', className)}>
      <div
        className={cn(
          ' relative w-3.5rem h-1.5rem flex items-center box-border border',
          photography ? 'bg-black border-white' : 'bg-white border-black',
          photography && (project ? '' : 'flex-row-reverse'),
          !photography && project && 'flex-row-reverse'
        )}
      >
        <div
          className={cn(
            'z-10 h-2rem w-1.5rem border',
            photography ? 'bg-white border-black' : 'bg-black border-white'
          )}
        ></div>
      </div>
    </div>
  )
}
