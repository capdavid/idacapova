import React from 'react'
import { ProjectsData } from 'utils/fetchContent'
import Image from 'next/image'
import { Video } from 'components'
import classNames from 'classnames'

interface MobileSectionProps {
  data: ProjectsData[]
  darkBackground?: boolean
}
export const MobileSection = ({ data, darkBackground }: MobileSectionProps) => {
  return (
    <div
      className={classNames('md:hidden flex flex-col gap-16 pt-16', darkBackground && 'bg-black')}
    >
      {data?.map(({ title: categoryTitle, projects }) =>
        projects.map(({ title, images }) => (
          <article className="w-full">
            <h3
              className={classNames(
                'whitespace-pre-wrap break-words text-14 mb-8 ml-8',
                darkBackground && 'text-white'
              )}
            >
              {categoryTitle} <span className="font-bold">{title}</span>
            </h3>
            <div className="flex flex-col w-full gap-8">
              {typeof images === 'object' ? (
                images?.map((img) => {
                  if (img.endsWith('mp4')) {
                    return (
                      <video
                        src={img}
                        autoPlay
                        muted
                        className="w-full max-h-1200 object-contain"
                      />
                    )
                  } else {
                    return <img className="w-full object-contain" src={img} alt="photo" />
                  }
                })
              ) : (
                <>
                  {images.endsWith('mp4') ? (
                    <Video
                      src={images}
                      autoPlay
                      muted
                      className="w-full max-h-1200 object-contain"
                    />
                  ) : (
                    <img className="w-full object-contain" src={images} alt="photo" />
                  )}
                </>
              )}
            </div>
          </article>
        ))
      )}
    </div>
  )
}
