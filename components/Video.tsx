import React, { useState, useRef } from 'react'
import Modal from 'react-modal'

interface VideoProps {
  src: string
  autoPlay?: boolean
  className?: string
  muted?: boolean
  blackBackground?: boolean
  unclickable?: boolean
}

export const Video: React.FC<VideoProps> = ({
  src,
  autoPlay,
  className,
  muted,
  unclickable,
  blackBackground,
}) => {
  const [play, setPlay] = useState(false)
  const videoRef = useRef<HTMLVideoElement>(null)
  if (!autoPlay) {
    if (play) {
      videoRef?.current?.play()
    } else {
      videoRef?.current?.pause()
    }
  }
  const [open, setOpen] = useState(false)

  if (open) {
    videoRef?.current?.pause()
  }
  return (
    <>
      <video
        src={src}
        loop
        muted={muted}
        className={className}
        autoPlay={autoPlay}
        onMouseOver={() => setPlay(true)}
        onMouseLeave={() => setPlay(false)}
        ref={videoRef}
        onClick={unclickable ? undefined : () => setOpen(true)}
      />
      {/* @ts-ignore */}
      <Modal
        isOpen={open}
        overlayClassName={{
          base: `modal ${blackBackground && 'bg-black'}`,
          afterOpen: 'afterOpen',
          beforeClose: 'beforeClose',
        }}
        ariaHideApp={false}
        className="h-9/10 max-w-9/10 max-h-1200 focus:outline-none flex justify-center items-center"
        onRequestClose={() => setOpen(false)}
      >
        <video controls autoPlay src={src} className="max-h-full max-w-full" />
      </Modal>
    </>
  )
}
