import React, { useState } from 'react'
import Modal from 'react-modal'

interface ImageProps {
  src: string
  className?: string
  blackBackground?: boolean
}

export const Image: React.FC<ImageProps> = ({ src, className, blackBackground }) => {
  const [open, setOpen] = useState(false)
  return (
    <>
      <img className={className} onClick={() => setOpen(true)} src={src} />
      {/* @ts-ignore */}
      <Modal
        isOpen={open}
        //class is defined in global.css
        overlayClassName={{
          base: `modal ${blackBackground && 'bg-black'}`,
          afterOpen: 'afterOpen',
          beforeClose: 'beforeClose',
        }}
        ariaHideApp={false}
        className="h-9/10 max-h-1200 max-w-9/10 focus:outline-none flex justify-center items-center"
        onRequestClose={() => setOpen(false)}
      >
        <img src={src} onClick={() => setOpen(false)} className="max-h-full max-w-full" />
      </Modal>
    </>
  )
}
