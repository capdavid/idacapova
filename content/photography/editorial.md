---
title: Fashion
date: 2021-11-11T22:20:00.000Z
projects:
  - title: Marionette | Editorial
    slug: marionette
    description: The editorial is inspired by the concept of the collection, French
      wooden marionettes.
    location: Prague, CZ
    summary: The editorial presents a graduate collection at UMRUM, by Alexandra
      Gnidiakova.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1623828663/idacapova/photography/Editorial/Marionette/_DSF5845_mkvbc4.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1623828663/idacapova/photography/Editorial/Marionette/_DSF5903_bk3nvk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1623828663/idacapova/photography/Editorial/Marionette/_DSF6028_bpt2lf.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1623828663/idacapova/photography/Editorial/Marionette/_DSF5774_f5mrd0.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1623828664/idacapova/photography/Editorial/Marionette/_DSF6149_grccma.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1623828664/idacapova/photography/Editorial/Marionette/_DSF6443_q1jcql.jpg
  - title: N7FAA52318 [RACHAEL] | Editorial
    slug: N7FAA52318-[RACHAEL]
    description: "Inspired by the dystopian concept of the collection and the
      charming character of Rachael, from the film Blade Runner. "
    description2: >
      Rachael was an artificially created and biologically enhanced person who
      believed she was human. 
    summary: The editorial presents a graduate collection at UMRUM, by Tomáš Němec.
    location: Prague, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629322706/idacapova/photography/Editorial/N7FAA52318%20%5BRACHAEL%5D/2-_DSF6062_kt0qtd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629322710/idacapova/photography/Editorial/N7FAA52318%20%5BRACHAEL%5D/5-_DSF6094_rkawnp.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629322700/idacapova/photography/Editorial/N7FAA52318%20%5BRACHAEL%5D/7-_DSF6207_dpm2gx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629322714/idacapova/photography/Editorial/N7FAA52318%20%5BRACHAEL%5D/3-_DSF6084_uufy7r.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629322706/idacapova/photography/Editorial/N7FAA52318%20%5BRACHAEL%5D/9-_DSF6345_dgj0tj.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629322711/idacapova/photography/Editorial/N7FAA52318%20%5BRACHAEL%5D/12-_DSF6491_nfjupv.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629322696/idacapova/photography/Editorial/N7FAA52318%20%5BRACHAEL%5D/1-_DSF5911_uvrjmr.jpg
  - title: Aleš Hnátek | Lookbook
    slug: ales-hnatek-lookbook
    description: Presentation of a collection by a Czech designer Aleš Hnátek.
    summary: With Gabriela Högerová as a model.
    location: Prague, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324282/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7603_yokgzr.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324283/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7692_rsgcha.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324284/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7784_yv7clc.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324282/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7612_kdlber.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324285/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7794_q77ss1.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324284/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7752_hlhkdk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324284/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7789_wznjay.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324283/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7721_qqilgv.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650324283/idacapova/photography/Editorial/Lookbook%20Ales/_DSF7660_nqjdcq.jpg
  - title: L'Écume des jours | Editorial
    slug: lecume-des-jours
    description: Studio fashion shoot for Valerie Jurčíková.
    summary: Creative direction Tomáš Němec, Camera assistant Karel Kolář, Make up
      Margarita Krupenich, Model 1 Eli Viktoria M. Enstrom, Model 2 Veronika
      Vašátová
    location: 2023, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661880/valerie10989_p2dard.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661883/valerie11821_amiakt.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661882/valerie11510_oxzffs.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661879/valerie10873_nnbped.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661881/valerie11320wide_bcuafz.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661884/valerie117472wide_whxhsp.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661879/valerie10866_z26aad.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661879/valerie11085_supdy9.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661879/valerie10724_czdawe.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661881/valerie11180_ejqesx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704661883/valerie11953_ooa7yx.jpg
---
