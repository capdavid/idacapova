---
title: BTS
date: 2021-01-24T22:38:00.000Z
projects:
  - title: WRN 22
    slug: wrn-22
    description: >+
      We Are Next is a fashion show representing young talent across Europe.
      These are some moments captured in the backstage at Bike Jesus.

    summary: A play with light.
    location: Prague, CZ, 2022
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941987/idacapova/photography/Backstage/We%27re%20next/_DSF1972_b8jiji.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941986/idacapova/photography/Backstage/We%27re%20next/_DSF1993_hcy9oq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941987/idacapova/photography/Backstage/We%27re%20next/_DSF2179_qmv6ow.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941988/idacapova/photography/Backstage/We%27re%20next/_DSF2236_uezgqk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941988/idacapova/photography/Backstage/We%27re%20next/_DSF2306_yraxia.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941993/idacapova/photography/Backstage/We%27re%20next/_DSF2551_hx8ivn.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941990/idacapova/photography/Backstage/We%27re%20next/_DSF2517_zyc9ee.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941993/idacapova/photography/Backstage/We%27re%20next/_DSF2762_jretoy.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632941992/idacapova/photography/Backstage/We%27re%20next/_DSF2730_mkirfl.jpg
  - title: Tomas Nemec FW22
    slug: tomas-nemec-fw22
    description: Preparations at Mercedes-Benz Prague fashion week, for a show by
      Tomáš Němec. A master student at UMPRUM and a winner of the Van Graaf
      Junior talent competition.
    description2: A winner collection of the Van Graaf Junior talent competition.
    summary: |+
      Van Graaf Junior talent presentation

    location: MBPWF, CZ, 2022
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650315884/idacapova/photography/Backstage/Van%20Graaf/_DSF8115_x2t8cz.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650315884/idacapova/photography/Backstage/Van%20Graaf/_DSF7980_i6rvml.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650315885/idacapova/photography/Backstage/Van%20Graaf/_DSF8023_aldijy.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650315879/idacapova/photography/Backstage/Van%20Graaf/_DSF8129_tmshm7.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1650315884/idacapova/photography/Backstage/Van%20Graaf/_DSF8102_tdv7zc.jpg
  - title: God save the queer
    slug: god-save-the-queer
    description: Graduate collection by arnau.ra. God save the queer represents his
      personal journey and the queer community around him.
    summary: A graduate at the IED Institute arnau.ra
    location: Barcelona. ES, 2022
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946836/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF0901_g9ftqr.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946834/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF0906_ixptmx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946837/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF0887_j5zjip.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946839/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF0716_h3mtsv.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946839/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF0769_lx37cr.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946836/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF1362_lsycqj.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946835/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF1314_cp90qx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1632946835/idacapova/photography/Backstage/God%20save%20the%20queer/_DSF1702_truvea.jpg
  - title: Mami Wati
    slug: mami-wati
    description: Photographs for Veronika Čechmánková diploma short film.
    location: "2023"
    summary: "In Prague "
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1714730353/_DSF9888_ogvfjy.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1714730350/_DSF9873_arvre2.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1714730345/_DSF9669_htlc42.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1714730350/_DSF9808_fk8ijk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1714730344/_DSF9916_hn5uam.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1714730341/_DSF9779_ibnj9k.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1714730338/_DSF9655_pyuu6q.jpg
---
