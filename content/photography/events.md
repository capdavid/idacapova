---
title: Events
date: 2021-01-25T22:38:00.000Z
projects:
  - title: London Pride
    slug: london-pride
    description: Digital photography series documenting Pride event on the streets
      of London.
    summary: Captured in 2019.
    location: London, UK
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611142/idacapova/photography/Events/London%20Pride/00000PORTRAIT_00000_BURST20190706162215686_1_2_xup9ji.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611142/idacapova/photography/Events/London%20Pride/IMG_20190706_175653_1_1_tvrwjd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611146/idacapova/photography/Events/London%20Pride/MVIMG_20190706_175438_1_1_bsnwva.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611142/idacapova/photography/Events/London%20Pride/00000PORTRAIT_00000_BURST20190706170106983_1_1_o5s4ut.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611143/idacapova/photography/Events/London%20Pride/IMG_20190706_165928_1_mtppyt.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611143/idacapova/photography/Events/London%20Pride/IMG_20190706_170000_1_p0oxzt.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611143/idacapova/photography/Events/London%20Pride/IMG_20190706_173037_1_2_ciaofo.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611143/idacapova/photography/Events/London%20Pride/IMG_20190706_180103_1_qaigll.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611144/idacapova/photography/Events/London%20Pride/MVIMG_20190706_162009_3_1_qnsunq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611611141/idacapova/photography/Events/London%20Pride/2019-09-05_05_1_myuhkp.jpg
  - title: Bendik Giske
    slug: bendik-giske
    description: A series of black and white photographs shot at a concert of Bendik Giske.
    summary: Organised by Dietl Archive / Polygon, 2019.
    location: Prague, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1625662157/idacapova/photography/Events/Bendik%20Giske/71044434_10211680611371220_7821282922308042752_n_1_rn2hlq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611610849/idacapova/photography/Events/Bendik%20Giske/70620249_10211680611571225_4879454843963441152_n_1_xebwts.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611610849/idacapova/photography/Events/Bendik%20Giske/2019-09-21_12_1_1_sntj6u.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1625661872/idacapova/photography/Events/Bendik%20Giske/2019-09-21_12_sxjrv2.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1625661871/idacapova/photography/Events/Bendik%20Giske/IMG_20190920_213042_wcfkzl.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1611610849/idacapova/photography/Events/Bendik%20Giske/MVIMG_20190920_213647_1_abcghz.jpg
  - title: Lunchmeat Festival
    slug: lunchmeat-festival
    description: Digital photography and videography series documenting an annual
      international festival dedicated to advanced electronic music and new
      media art.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1625662432/idacapova/photography/Events/Lunchmeat%20Festival/DSC07556_gokfll.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1613679151/idacapova/photography/Events/Lunchmeat%20Festival/rsz_dsc07427_1_kilh71.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1625662432/idacapova/photography/Events/Lunchmeat%20Festival/DSC07537_uvtwfu.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1613679151/idacapova/photography/Events/Lunchmeat%20Festival/rsz_dsc07543_1_bthbe2.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1613679151/idacapova/photography/Events/Lunchmeat%20Festival/rsz_dsc02765_1_1_renaww.jpg
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1613732668/idacapova/photography/Events/Lunchmeat%20Festival/l1_v3k28p.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1613732655/idacapova/photography/Events/Lunchmeat%20Festival/sophie_uqyuxb.mp4
    location: Prague, CZ
    description2: ""
    summary: Captured in September 2020.
  - title: The People of Kolonie
    slug: the-people-of-kolonie
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604060/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7058_mfamdx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604092/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6968_ssxrv3.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604087/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6998_jhsgsm.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604091/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6890_aylwxe.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604086/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6990_uiedsk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604081/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6960_sewkpp.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604076/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7030_v9yntc.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604069/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6913_ifcykw.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604089/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6938_m4imlv.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604070/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF6976_zzeyb4.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604077/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7012_ajsi67.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604045/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7072_all8dg.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604045/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7078_a7ct1n.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604041/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7101_rzm3cc.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604051/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7102_croxdd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604073/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7104_ingya1.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604058/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7109_b2qtxn.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604055/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7127_rpsxvo.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604035/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7133_uvfmte.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1626604029/idacapova/photography/Events/The%20People%20of%20Kolonie/_DSF7122_pan4ab.jpg
    description: A series depicting the people attending the event Kolonie. Their
      euphoria, friendships, happiness, beauty, tiredness and overall glory.
    summary: Organised by Cukr, 2021
    location: Skanzen Řeporyje, CZ
  - title: Transforma '21
    slug: transforma-21
    description: Series capturing the Transforma festival.
    summary: Taken in July 2021.
    location: Tábor, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641329941/idacapova/photography/Events/Transforma/_DSF9626_rigbcu.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641329941/idacapova/photography/Events/Transforma/_DSF9655_ythewx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641329941/idacapova/photography/Events/Transforma/_DSF9627_wncmmf.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641329939/idacapova/photography/Events/Transforma/_DSF9231_lul5ux.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641329941/idacapova/photography/Events/Transforma/_DSF9297_utirdj.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641329940/idacapova/photography/Events/Transforma/_DSF9724_wezam3.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641329940/idacapova/photography/Events/Transforma/_DSF9735_jljv9w.jpg
  - title: Till the Last
    slug: till-the-last
    description: Event organised by Lunchmeat.  An international festival dedicated
      to advanced electronic music and new media art.
    location: Kladno, CZ
    summary: Captured in 2021.
    description2: ""
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324984/idacapova/photography/Events/Till%20the%20last/7-_DSF8420_2_me8o12.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324982/idacapova/photography/Events/Till%20the%20last/8-_DSF8426_int5hd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324987/idacapova/photography/Events/Till%20the%20last/10-_DSF8555_shmblb.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324994/idacapova/photography/Events/Till%20the%20last/13-_DSF8688_m1r6cm.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324976/idacapova/photography/Events/Till%20the%20last/14-_DSF8727_fef3ug.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324979/idacapova/photography/Events/Till%20the%20last/16-_DSF8743_vdwd2k.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324981/idacapova/photography/Events/Till%20the%20last/15-_DSF8736_u4s4vs.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324983/idacapova/photography/Events/Till%20the%20last/17-_DSF8776_hej21n.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324990/idacapova/photography/Events/Till%20the%20last/18-_DSF8820_ijvy4h.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324989/idacapova/photography/Events/Till%20the%20last/19-_DSF8828_dfeolz.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324989/idacapova/photography/Events/Till%20the%20last/21-_DSF8880_whbi4e.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629324996/idacapova/photography/Events/Till%20the%20last/22-_DSF8884_ut3nkg.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629325000/idacapova/photography/Events/Till%20the%20last/23-_DSF8960_pcgmxk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629325002/idacapova/photography/Events/Till%20the%20last/24-_DSF9017_fxsnbk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629325007/idacapova/photography/Events/Till%20the%20last/25-_DSF9037_pt22s5.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629325002/idacapova/photography/Events/Till%20the%20last/26-_DSF9051_xit3us.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629325007/idacapova/photography/Events/Till%20the%20last/27-_DSF9055_xqjdyd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629325005/idacapova/photography/Events/Till%20the%20last/28-_DSF9068_prso4l.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1629325008/idacapova/photography/Events/Till%20the%20last/29-_DSF9132_tuz2vc.jpg
  - title: Oliver Torr - Fragility of Context vinyl release
    slug: oliver-torr-fragility-of-context-vinyl-release
    description: "Photographs documenting a release party for Oliver Torr's new
      album Fragility of Context. "
    location: Fuchs2, Prague, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663246845/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4393_ywy6l1.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663246844/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4647_hfn9il.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663246845/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4400_b73lkx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1673042107/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4618_krisan.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1673042106/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4534_mmqi0d.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1673042106/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4524_p6jpdv.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1673042109/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4606_zvnqwq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1673042108/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4701_ui4gsy.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1673042107/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4434_em4syo.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663246844/idacapova/photography/Events/Oliver%20Torr%20-%20Fragility%20of%20Context%20vinyl%20release/_DSF4540_axnrcf.jpg
    summary: Taken in November 2021.
  - title: Transforma '22
    slug: transforma-22
    description: Series documenting the Transforma festival in summer of 2022
    summary: Taken in June 2022.
    location: Tábor, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247950/idacapova/photography/Events/Tranforma%2022/_DSF1079_jkqa1t.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247950/idacapova/photography/Events/Tranforma%2022/_DSF0825_ujz8ec.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247948/idacapova/photography/Events/Tranforma%2022/_DSF0957_av5xo2.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247952/idacapova/photography/Events/Tranforma%2022/_DSF1257_xhptgn.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247953/idacapova/photography/Events/Tranforma%2022/_DSF1384_uozz0s.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247953/idacapova/photography/Events/Tranforma%2022/_DSF1429_g5a68a.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247954/idacapova/photography/Events/Tranforma%2022/_DSF1455_cc3iyn.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663250854/idacapova/photography/Events/Tranforma%2022/rsz__dsf1346_fp0fmp.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663249977/idacapova/photography/Events/Tranforma%2022/rsz_1_dsf1092_fq5wgh.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247955/idacapova/photography/Events/Tranforma%2022/_DSF1554_i2xshn.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663247955/idacapova/photography/Events/Tranforma%2022/_DSF1559_wm7omg.jpg
  - title: Bvcolica
    slug: bvcolica
    description: >
      Bvcolica is an annual forest festival of experimental music, promoting
      respect, love of nature, diversity and solidarity.
    location: Přelouč, CZ
    summary: Taken on on film in August 2022.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081072/idacapova/photography/Events/Bvcolica/1-R1-00388-0002_ci5092.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081127/idacapova/photography/Events/Bvcolica/2-R1-00389-024A_nemo4c.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081544/idacapova/photography/Events/Bvcolica/rsz_8-r1-00388-0006_w5epuq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081124/idacapova/photography/Events/Bvcolica/9-R1-00388-0008_uaa89m.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081083/idacapova/photography/Events/Bvcolica/14-R1-00388-0027_ebztll.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081080/idacapova/photography/Events/Bvcolica/12-R1-00388-0017_nyzc0h.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1666270673/idacapova/photography/Events/Bvcolica/R1-00388-0023_asi0wi.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081084/idacapova/photography/Events/Bvcolica/15-R1-00388-0032_lrkqlq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081080/idacapova/photography/Events/Bvcolica/6-R1-00389-023A_nfpo5k.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663081086/idacapova/photography/Events/Bvcolica/17-R1-00389-017A_hucbuq.jpg
---
