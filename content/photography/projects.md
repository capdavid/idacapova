---
title: Projects
date: 2023-12-18T12:27:17.975Z
projects:
  - title: Identity
    slug: identity
    description: Project comprised of snapshots of young people. Capturing their
      serenity, vulnerability, expressiveness, diversity, absence and coolness.
    description2: The series aims to represent young misfits of society.
    summary: Taken between 2019 and 2020.
    location: Created as a part of graduation project at Amsterdam Fashion Institute.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328898/idacapova/photography/Projects/Identity/IMG_20191108_124250_510_1_vxjexe.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328860/idacapova/photography/Projects/Identity/MVIMG_20190721_065003_1_rs1wrd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328898/idacapova/photography/Projects/Identity/2019-09-05_05_2_e0z7un.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328898/idacapova/photography/Projects/Identity/DSC05689_1_ohzvhi.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328901/idacapova/photography/Projects/Identity/received_434092923813988_1_h2ioog.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328895/idacapova/photography/Projects/Identity/2019-09-05_05_2_1_fd2jnn.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328903/idacapova/photography/Projects/Identity/IMG_20191129_230924_934_1_i2emtp.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328910/idacapova/photography/Projects/Identity/DSC05114_1_uwfoko.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328895/idacapova/photography/Projects/Identity/received_188463768697490_1_sptm4f.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328872/idacapova/photography/Projects/Identity/MVIMG_20191031_140948_1_gr6a96.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641328902/idacapova/photography/Projects/Identity/DSC05786_1_u9qxt8.jpg
  - title: A dream you wanted to dream
    description: Series of images collected over 3 years in Czech Republic and The
      Netherlands.
    slug: a-dream-you-wanted-to-dream
    description2: The project contemplates the idea of being. It tells a story of a
      life caught in a dream.
    summary: Taken between 2019 and 2021 on film.
    location: The Netherlands and Czech Republic.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1637456855/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./R1-06340-031A_vv8r8n.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1641862334/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./rsz_11cesta_hsmdq9.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1637456856/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./R1-06340-034A_f3jtpj.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1637456852/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./R1-04552-000A_r3pmoy.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1637456851/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./R1-09634-0016_roqnq8.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1637456853/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./_DSF5021_cbpkyu.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1637456850/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./2019-09-10_05_zeuv31.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1666271620/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./r1-09634-0037_zrizfc.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1639869809/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./rsz_21r1-00090-012b_i4p3io.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1639869808/idacapova/photography/Projects/Dream%2C%20you%20wanted%20to%20dream./Graphic_design_ve3jse.jpg
  - title: Noc, blízkost, stav bez tíže
    slug: noc-blizkost-stav-bez-tize
    description: Lo-fi documentary cycle savouring the charm of youth nightlife.
    summary: 2022-2023
    location: Luleč, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474494/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_7JPG_d3an7w.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474495/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_1_mgcrzd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474501/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_10_sgsrig.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474496/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_6_a62ocw.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474500/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_4_yeccjd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474499/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_9_ehmgdl.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474503/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_8_ebhqhp.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1678474503/idacapova/photography/Projects/NOc%2C%20blizkost%2C../Noc__bli%CC%81zkost__stav_bez_ti%CC%81z%CC%8Ce_12_i2dthj.jpg
  - title: Památkově chráněno
    slug: pamatkove-chraneno
    description: Project created as a reaction to ad placements on UNESCO and
      national heritage protected buildings in Prague.
    description2: The series aims to point to the issue in a playful manner, by
      combining parts of buildings on top of each other and creating a city
      block.
    summary: Created from 4 x 6 inch blow - up photographs built like a puzzle on
      top and next to each other.
    location: Prague, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1673812733/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/rsz_pama%CC%81tkove%CC%8C_chra%CC%81ne%CC%8Cno_cer59o.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816699/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_18.58.12_avjdiv.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816702/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_18.58.59_tcwrov.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816703/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_18.59.12_gvwgy1.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816702/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_18.59.38_ut2bsh.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816702/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_18.59.49_htnjja.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816703/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_19.00.02_tyl1no.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816702/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_19.00.17_ooxz7e.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816700/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_19.00.32_jzfjsh.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1664816702/idacapova/photography/Projects/Pam%C3%A1tkov%C4%9B%20chr%C3%A1n%C4%9Bno/Screenshot_2022-10-03_at_19.00.42_s4sy4q.jpg
  - title: Jeden rok, osm měsíců a několik dní
    slug: jeden-rok-osm-mesicu-a-nekolik-dni
    description: Series of analog photographs capturing a room and what was left
      after a year, eight months and a couple of days
    summary: "2022"
    location: Škrétova 42/2, Prague, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698090590/idacapova/photography/Projects/Jeden%20rok%2C../R1-02005-019A_abkbcy.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698090604/idacapova/photography/Projects/Jeden%20rok%2C../R1-02005-004A_h0bz1i.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698090608/idacapova/photography/Projects/Jeden%20rok%2C../R1-02005-015A_wtkswq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698090599/idacapova/photography/Projects/Jeden%20rok%2C../R1-02005-020A_tqno9y.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698090600/idacapova/photography/Projects/Jeden%20rok%2C../R1-02005-011A_phcahz.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698090576/idacapova/photography/Projects/Jeden%20rok%2C../Jeden_rok_dnwkxw.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698090608/idacapova/photography/Projects/Jeden%20rok%2C../R1-02005-024A_odmail.jpg
  - title: Swans are to be cherished, not harvested
    slug: swans-are-to-be-cherished-not-harvested
    location: Prague, CZ
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1702910322/9-_31A2247_l2benl.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1702910232/3-_31A2307_qylcg9.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1702913187/4-_31A2282_ohxjdf.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704660143/R1-00400-0011_copy_bitbdo.jpg
    description: "Swans are to be cherished, not harvested. For they will give you
      in return what none else will. "
    description2: ""
    summary: The series reflects on female beauty, grace and everyday sorrows.
  - title: Bez Něho by to nedokázala
    slug: bez-neho-bych-to-nedokazala
    description: Examining a father/daughter relationship.
    description2: "Pondering over a sentence my mother mentioned in relation to her
      marriage and relationship with my father. "
    summary: Without God, I would not have the love to stay.
    location: Showcased as part of JFK exhibit in October 2023 in Prague.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698072562/idacapova/photography/Projects/Hory%20p%C5%99en%C3%A1%C5%A1%C3%AD/R1-09022-0005_q2vs3s.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698072557/idacapova/photography/Projects/Hory%20p%C5%99en%C3%A1%C5%A1%C3%AD/_DSF3152_gzhiiq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698072571/idacapova/photography/Projects/Hory%20p%C5%99en%C3%A1%C5%A1%C3%AD/koren_u39kxl.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698072574/idacapova/photography/Projects/Hory%20p%C5%99en%C3%A1%C5%A1%C3%AD/_DSF0058_x6cwah.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698072563/idacapova/photography/Projects/Hory%20p%C5%99en%C3%A1%C5%A1%C3%AD/Untitled_design_e2noif.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1698087977/idacapova/photography/Projects/Hory%20p%C5%99en%C3%A1%C5%A1%C3%AD/rsz_1img_2441_jdeov7.jpg
  - title: Cease to know
    description: Cease to know or to tell or to see or to be your own.
    slug: cease-to-know
    location: CZ
    summary: Series depicts the state of deep depression, being completely paralyzed
      and unable to help yourself.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1704660759/R1-05834-025A_copy_vgkson.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1702910331/13-IMG_3295_uln6bh.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1707148027/rsz_1uzkost_rjm5tc.jpg
---
