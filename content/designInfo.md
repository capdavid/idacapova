---
date: 2021-01-31T14:15:11.276Z
aboutDescription: "Originating from Fashion and Design, Ida Capova’s graphic
  design has a playful and clean nature. "
aboutDescription2: Embracing concepts of the new designed with authenticity and sentiment.
about-summary: Creating contemporary designs since 2018.
aboutLocation: Based in Prague, CZ.
aboutImage: https://res.cloudinary.com/djwjnsrw5/image/upload/v1632942769/idacapova/photography/AirBrush_20210912181601_elnsaf.jpg
contactName: IDA CAPOVA
contactEmail: capovaida@gmail.com
contactInstagram: "@idacapova"
aboutSummary: Creating contemporary designs since 2018.
---
