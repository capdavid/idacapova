---
date: 2021-01-31T14:15:11.276Z
aboutDescription: "Born in 1995 in the capital of Czech Republic, Prague.
  Graduated in Fashion and Design at Amsterdam Fashion Institute in 2021. "
aboutDescription2: Now working as a BTS photographer and costume assistant in film.
about-summary: Capturing the essence of people, places and everyday moments with
  subjective authenticity and poetic beauty. Images accompanied with tender
  quietness.
aboutLocation: Based in Prague, CZ.
aboutImage: https://res.cloudinary.com/djwjnsrw5/image/upload/v1632942769/idacapova/photography/AirBrush_20210912181601_elnsaf.jpg
contactName: IDA CAPOVA
contactEmail: capovaida@gmail.com
contactInstagram: "@idacapova_"
aboutSummary: Catching glimpses of life since 2018.
---
