---
title: Logotype
date: 2021-02-08T13:35:01.486Z
projects:
  - title: Nahá studio
    slug: naha-studio
    description: Logo created as a proposal for a starting clothing brand Nahá studio.
    location: Prague, CZ
    images: https://res.cloudinary.com/djwjnsrw5/image/upload/v1612877138/idacapova/Graphic%20Design/Logotype/Naha%20studio/rsz_logo_naha_1_uqizkq.jpg
    summary: In collaboration with Kristýna Hrabánková.
  - title: Babez on Fire
    slug: babez-on-fire
    description: Logo for a young, futuristic start up, that curates and represents
      up and coming artist from different background and fields.
    description2: "The company aims to shift perspectives on judgement and stereotypes. "
    summary: "The company aims to shift perspectives on judgement and stereotypes. "
    location: >
      Developed as part of a graduation project at Amsterdam Fashion Institute.  
    images:
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1619650067/idacapova/Graphic%20Design/Logotype/Babez%20on%20Fire/Logo_final_upload_u2enzh.mp4
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612821720/idacapova/Graphic%20Design/Logotype/Babez%20on%20Fire/Babez_logo3_1_txjsyz.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1619686327/idacapova/Graphic%20Design/Logotype/Babez%20on%20Fire/rsz_matches_new_phfoag.jpg
---
