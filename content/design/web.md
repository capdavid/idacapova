---
title: Web
date: 2021-02-08T13:34:00.000Z
projects:
  - title: Prague 2
    slug: prague-2
    description: "Mock up of a web design for the city district of Prague 2. "
    description2: The challenge was to create a website without imagery that still
      looks lively and is easy to use.
    summary: "The aim was to create a web design for this portal that is easy to
      follow, doesn’t have unnecessary information and is visually pleasing for
      the user as well as playful. "
    location: Self inspired project.
    images:
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1617882062/idacapova/Graphic%20Design/Web%20design/Praha%202/P2_web_presentation_final_mikllv.mp4
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1617878793/idacapova/Graphic%20Design/Web%20design/Praha%202/2_otm3wg.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1617986822/idacapova/Graphic%20Design/Web%20design/Praha%202/3_alluui.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1617986824/idacapova/Graphic%20Design/Web%20design/Praha%202/4_g26cqx.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1617986825/idacapova/Graphic%20Design/Web%20design/Praha%202/1_p4hoqe.jpg
---
