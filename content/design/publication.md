---
title: Publication
date: 2021-02-08T14:13:45.579Z
projects:
  - title: Journal
    slug: journal
    description: "Excerpts from a printed publication documenting the life,
      sceneries and palettes of a Canary island called Furteventura. "
    summary: A self inspired project put together from a series of photographs taken
      on the island.
    location: The layout and graphics work to underpin its sensibility in a
      contemporary way.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793940/idacapova/Graphic%20Design/Publication/Journal/rsz_untitled-3_copy_zez3nk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612876929/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO2_2_vk95pk.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793913/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO4_vfz6d5.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612876929/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO3_yoewbi.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612876934/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO5_izwuah.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612876934/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO6_vlqcqg.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793919/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO7_b9jozd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612876931/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO8_rj6hqq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793920/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO9_b3ecce.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793912/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO12_t7xexo.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793916/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO13_pqagss.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793918/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO16_lbcduw.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793920/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO17_mizykq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793914/idacapova/Graphic%20Design/Publication/Journal/CORRALEJO18_r46h8g.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612793918/idacapova/Graphic%20Design/Publication/Journal/rsz_untitled-3_tdvdzb.jpg
  - title: e/Zine
    slug: ezine
    description: Excerpts from an online publication on the topic of IDENTITY.
    description2: The objective was to present the reader with a multi-sensory
      experience, through interactive layouts, sound accompanied pages and
      visually stimulating content.
    summary: The e/Zine aims shift perspectives on judgement and stereotypes and
      introduce a world of Expressive diversity, Serene vulnerability and Absent
      cool.
    location: "Created as part of a graduation project at Amsterdam Fashion Institute.  "
    images:
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798284/idacapova/Graphic%20Design/Publication/eZine/entry_zbz89k.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798477/idacapova/Graphic%20Design/Publication/eZine/Nataliya_mpzbsk.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798496/idacapova/Graphic%20Design/Publication/eZine/Nataliya_project_fyfv27.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798438/idacapova/Graphic%20Design/Publication/eZine/kids_cniy32.mp4
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612798715/idacapova/Graphic%20Design/Publication/eZine/Screenshot_2021-01-12_at_20.53.28_uqd6sy.jpg
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798478/idacapova/Graphic%20Design/Publication/eZine/V_project_ioa0bd.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798471/idacapova/Graphic%20Design/Publication/eZine/sun_cnzbyo.mp4
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612798696/idacapova/Graphic%20Design/Publication/eZine/Screenshot_2021-02-08_at_15.52.13_yqrwcw.jpg
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798276/idacapova/Graphic%20Design/Publication/eZine/Bendik_work_sirje3.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798420/idacapova/Graphic%20Design/Publication/eZine/masked_tkmgus.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798299/idacapova/Graphic%20Design/Publication/eZine/absent_cool_pvkyv8.mp4
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612798699/idacapova/Graphic%20Design/Publication/eZine/Screenshot_2021-01-11_at_18.27.44_ilxzxe.jpg
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798276/idacapova/Graphic%20Design/Publication/eZine/asher_agnn6j.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798284/idacapova/Graphic%20Design/Publication/eZine/handball_bfsep4.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798514/idacapova/Graphic%20Design/Publication/eZine/Skywalker_pg2rch.mp4
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612798699/idacapova/Graphic%20Design/Publication/eZine/Screenshot_2021-01-11_at_18.33.15_n6lkee.jpg
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798282/idacapova/Graphic%20Design/Publication/eZine/credits_ml99tl.mp4
      - https://res.cloudinary.com/djwjnsrw5/video/upload/v1612798426/idacapova/Graphic%20Design/Publication/eZine/cover_okxd3s.mp4
---
