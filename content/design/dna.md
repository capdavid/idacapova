---
title: Dna
date: 2021-02-08T13:54:22.048Z
projects:
  - title: Céline x Ida Capova
    slug: celine-x-ida-capova
    description: "Brand identity and concept created as an imagined creative
      director of a fashion house. "
    description2: "Constructed to preserve its heritage combined with an updated,
      contemporary vision, mission and values. "
    summary: Addressing issues of diversity, feminism and equality.
    location: Project developed at Amsterdam Fashion Institute.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612877691/idacapova/Graphic%20Design/DNA/Celine%20x%20Ida%20Capova/rsz_screenshot_2021-01-14_at_1456_2_ziscqs.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612792614/idacapova/Graphic%20Design/DNA/Celine%20x%20Ida%20Capova/rsz_screenshot_2021-01-14_at_145603_e7lbaq.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612792616/idacapova/Graphic%20Design/DNA/Celine%20x%20Ida%20Capova/rsz_3screenshot_2021-02-08_at_141911_sribgt.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1707151119/navrhy_collection_amfi_rsz2_shskve.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1707151118/navrhy_collection_amfi_rsz_bqv1e1.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1707151211/AMFI_SPEC19_0916_rhfcsh.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1707151207/AMFI_SPEC19_0920_ipv4fd.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1707151751/_89A4449_copy_xnn037.jpg
  - title: Babez on Fire
    slug: babez-on-fire
    description: Brand, its identity and visual language created for a young and
      futuristic start up, that curates and represents up and coming artist from
      different background and fields.
    summary: "Established company aims to shift perspectives on judgement and
      stereotypes. "
    location: "Developed as part of a graduation project at Amsterdam Fashion
      Institute.  "
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612792890/idacapova/Graphic%20Design/DNA/Babez%20on%20Fire/rsz_screenshot_2021-01-13_at_2156_1_wdpuwu.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612792890/idacapova/Graphic%20Design/DNA/Babez%20on%20Fire/rsz_screenshot_2021-01-13_at_2157_1_xplkwm.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612877093/idacapova/Graphic%20Design/DNA/Babez%20on%20Fire/1_gibm3l.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1612792890/idacapova/Graphic%20Design/DNA/Babez%20on%20Fire/rsz_screenshot_2021-01-13_at_2158_1_texxsb.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1613693000/idacapova/Graphic%20Design/DNA/Babez%20on%20Fire/rsz_ezinepic_1_qpknsp.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1613693000/idacapova/Graphic%20Design/DNA/Babez%20on%20Fire/rsz_4instagram_dcsxj5.jpg
---
