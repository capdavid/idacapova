---
title: Posters
date: 2021-02-08T13:36:00.000Z
projects:
  - title: "Prague spring "
    slug: prague-spring
    description: Ideas for posters of an international music festival in Prague.
      Each year the public is presented with outstanding performing artists,
      symphony orchestras and chamber music ensembles of the world.
    summary: >
      The posters are made to catch your eye and make you feel the intensity of
      the craft.
    location: Made in Prague.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1618577496/idacapova/Graphic%20Design/Posters/Prague%20Spring/PRG_spring_festival_posters_final_xqnpec.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1618577497/idacapova/Graphic%20Design/Posters/Prague%20Spring/PRG_spring_festival_posters_final2_cefhgr.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1618577497/idacapova/Graphic%20Design/Posters/Prague%20Spring/PRG_spring_festival_posters_final3_xc9vt8.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1618577499/idacapova/Graphic%20Design/Posters/Prague%20Spring/poster_on_the_street_resized_z3jage.jpg
    description2: Created in collaboration with Petra Hajska photography.
  - title: Tomáš Němec MBPFW Fashion show
    slug: tomas-nemec-mbpfw-fashion-show
    summary: Creative Direction - Tomáš Němec, Model - Alexandra Gnidiaková,
      Photographer - Ida Čápová, 2022
    location: Prague, CZ
    description: A poster for a fashion show of Tomáš Němec. The inspiration for his
      collection was a Czech film from the 60's - Dáma na kolejích ( Lady on the
      tracks ), 1966.
    images:
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663084299/idacapova/Graphic%20Design/Posters/Tom%C3%A1%C5%A1%20N%C4%9Bmec/rsz_final_1_rgb_dkmeos.jpg
      - https://res.cloudinary.com/djwjnsrw5/image/upload/v1663084299/idacapova/Graphic%20Design/Posters/Tom%C3%A1%C5%A1%20N%C4%9Bmec/rsz_final_2_rgb_xyslot.jpg
---
