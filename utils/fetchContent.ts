import fs from 'fs'
import path from 'path'
import yaml from 'js-yaml'
import matter from 'gray-matter'

export interface Project {
  description: string
  description2?: string
  summary: string
  location: string
  images: string[] | string
  title: string
  slug: string
}

export interface ProjectsData {
  title: string
  name: string
  date: string
  projects: Project[]
}

export function fetchContent(directory: string): ProjectsData[] {
  const folder = path.join(process.cwd(), `content/${directory}`)
  const fileNames = fs.readdirSync(folder)
  const data = fileNames
    .filter((fileName) => fileName.endsWith('.md'))
    .map((fileName) => {
      const filePath = path.join(folder, fileName)
      const fileContent = fs.readFileSync(filePath)
      const parsed = matter(fileContent, {
        engines: {
          yaml: (s) => yaml.load(s, { schema: yaml.JSON_SCHEMA }) as Record<string, unknown>,
        },
      })

      parsed.data.name = parsed.data.title.toLowerCase()
      parsed.data.projects.reverse()

      const data = parsed.data as ProjectsData
      return data
    })
  const sortedData = data.sort((a, b) => (b.date < a.date ? -1 : b.date > a.date ? 1 : 0))
  return sortedData
}
